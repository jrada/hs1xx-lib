# hs1xx-lib

**Library for TP-Link HS100 & HS110 Smart Plug** \
\
Usage:
```java
public static void main(String[] args) {
    try {

        PlugClient plugClient = new PlugClient("<your_plug_ip>");
        HS110Plug plug = new HS110Plug(plugClient);

        GetSysinfoResponse plugInfo = plug.getInfo();
        System.out.println(plugInfo.getJsonString());

    } catch (IOException | PlugException ex) {
        LOG.error("An error occurred", ex);
    }
}
```
see Main.java