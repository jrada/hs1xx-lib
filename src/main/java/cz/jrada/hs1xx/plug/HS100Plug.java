package cz.jrada.hs1xx.plug;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.jrada.hs1xx.plug.exceptions.PlugException;
import cz.jrada.hs1xx.plug.exceptions.UnexpectedPlugResponseException;
import cz.jrada.hs1xx.plug.responses.PlugResponse;
import cz.jrada.hs1xx.plug.responses.system.getSysinfo.GetSysinfoResponse;
import cz.jrada.hs1xx.plug.responses.system.setRelayState.SetRelayStateResponse;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TP-Link HS100 Smart Plug
 * Info: https://www.softscheck.com/en/reverse-engineering-tp-link-hs110/
 *
 * @author Jan Rada - jrada.cz@gmail.com
 */
public class HS100Plug {

    private static final Logger LOG = LoggerFactory.getLogger(HS100Plug.class);

    // HS100 commands =================================================================================================
    // All available commands: https://github.com/softScheck/tplink-smartplug/blob/master/tplink-smarthome-commands.txt

    // system commands ================================================================================================
    // Overall info
    protected static final String CMD_SYSTEM_GET_INFO = "{\"system\":{\"get_sysinfo\":{}}}";
    // Turn on
    protected static final String CMD_SYSTEM_TURN_ON = "{\"system\":{\"set_relay_state\":{\"state\":1}}}}";
    // Turn off
    protected static final String CMD_SYSTEM_TURN_OFF = "{\"system\":{\"set_relay_state\":{\"state\":0}}}}";
    // Reboot device
    protected static final String CMD_SYSTEM_REBOOT = "{\"system\":{\"reboot\":{\"delay\":1}}}";
    // Factory reset
    protected static final String CMD_SYSTEM_RESET = "{\"system\":{\"reset\":{\"delay\":1}}}";

    // cloud commands =================================================================================================
    // Cloud connectivity info
    protected static final String CMD_CLOUD_GET_INFO = "{\"cnCloud\":{\"get_info\":null}}";

    protected final PlugClient plugClient;
    protected final ObjectMapper objectMapper;

    public HS100Plug(PlugClient plugClient) {
        this.plugClient = plugClient;
        this.objectMapper = new ObjectMapper();
    }

    public SetRelayStateResponse turnOn() throws IOException, PlugException {
        String jsonResponse = plugClient.sendCommand(CMD_SYSTEM_TURN_ON);
        return parseJsonResponse(jsonResponse, SetRelayStateResponse.class);
    }

    public SetRelayStateResponse turnOff() throws IOException, PlugException {
        String jsonResponse = plugClient.sendCommand(CMD_SYSTEM_TURN_OFF);
        return parseJsonResponse(jsonResponse, SetRelayStateResponse.class);
    }

    public boolean isTurnedOn() throws IOException, PlugException {
        int relayState = getInfo().getSystem().getGetSysinfo().getRelayState();
        return PlugRelayState.getRelayState(relayState) == PlugRelayState.ON;
    }

    public GetSysinfoResponse getInfo() throws IOException, PlugException {
        String jsonResponse = plugClient.sendCommand(CMD_SYSTEM_GET_INFO);
        return parseJsonResponse(jsonResponse, GetSysinfoResponse.class);
    }

    public String reboot() throws IOException, PlugException {
        return plugClient.sendCommand(CMD_SYSTEM_REBOOT);
    }

    public String reset() throws IOException, PlugException {
        return plugClient.sendCommand(CMD_SYSTEM_RESET);
    }

    public String getCloudInfo() throws IOException, PlugException {
        return plugClient.sendCommand(CMD_CLOUD_GET_INFO);
    }

    protected <T extends PlugResponse> T parseJsonResponse(String jsonResponse, Class<T> type) throws UnexpectedPlugResponseException {
        try {
            T response = objectMapper.readValue(jsonResponse, type);
            response.setStringValue(jsonResponse);
            return response;
        }
        catch(IOException | IllegalArgumentException ex) {
            throw new UnexpectedPlugResponseException(ex);
        }
    }

    public static enum PlugRelayState {
        ON(1), OFF(0), UNKNOWN(Integer.MAX_VALUE);

        private final int relayStateCode;

        private PlugRelayState(int relayStateCode) {
            this.relayStateCode = relayStateCode;
        }

        public static PlugRelayState getRelayState(int relayStateCode) {
            for (PlugRelayState plugRelayState : PlugRelayState.values()) {
                if (plugRelayState.relayStateCode == relayStateCode) {
                    return plugRelayState;
                }
            }

            LOG.warn("Unknown plug relay state: relayStateCode={}", relayStateCode);
            return UNKNOWN;
        }
    }

}
