package cz.jrada.hs1xx.plug;

import cz.jrada.hs1xx.plug.exceptions.PlugException;
import cz.jrada.hs1xx.plug.responses.emeter.getRealtime.GetRealtimeResponse;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TP-Link HS110 Smart Plug
 * Info: https://www.softscheck.com/en/reverse-engineering-tp-link-hs110/
 *
 * @author Jan Rada - jrada.cz@gmail.com
 */
public class HS110Plug extends HS100Plug {

    private static final Logger LOG = LoggerFactory.getLogger(HS110Plug.class);

    // HS110 commands =================================================================================================
    // All available commands: https://github.com/softScheck/tplink-smartplug/blob/master/tplink-smarthome-commands.txt

    // emeter commands ================================================================================================
    // Get realtime emeter data
    protected static final String CMD_EMETER_REALTIME = "{\"emeter\":{\"get_realtime\":{}}}";
    // Get daily stats for current month
    protected static final String CMD_EMETER_DAYSTAT = "{\"emeter\":{\"get_daystat\":{\"month\":%d, \"year\":%d}}}";
    // Get daily stats for current month
    protected static final String CMD_EMETER_MONTHSTAT = "{\"emeter\":{\"get_monthstat\":{\"year\":%d}}}";
    // Erase emeter stats
    protected static final String CMD_EMETER_ERASE_EMETER_STATS = "{\"emeter\":{\"erase_emeter_stat\":{}}}";

    // schedule commands ==============================================================================================
    // Erase runtime stats
    protected static final String CMD_SCHEDULE_ERASE_RUNTIME_STATS = "{\"schedule\":{\"erase_runtime_stat\":{}}}";

    public HS110Plug(PlugClient plugClient) {
        super(plugClient);
    }

    public GetRealtimeResponse getEmeterRealtime() throws IOException, PlugException {
        String jsonResponse = plugClient.sendCommand(CMD_EMETER_REALTIME);
        return parseJsonResponse(jsonResponse, GetRealtimeResponse.class);
    }

    public String getDaystat(int month, int year) throws IOException, PlugException {
        return plugClient.sendCommand(String.format(CMD_EMETER_DAYSTAT, month, year));
    }

    public String getMonthstat(int year) throws IOException, PlugException {
        return plugClient.sendCommand(String.format(CMD_EMETER_MONTHSTAT, year));
    }

    public String eraseEmeterStats() throws IOException, PlugException {
        return plugClient.sendCommand(CMD_EMETER_ERASE_EMETER_STATS);
    }

    public String eraseRuntimeStats() throws IOException, PlugException {
        return plugClient.sendCommand(CMD_SCHEDULE_ERASE_RUNTIME_STATS);
    }

}
