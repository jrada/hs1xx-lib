package cz.jrada.hs1xx.plug;

import cz.jrada.hs1xx.plug.exceptions.PlugException;
import cz.jrada.hs1xx.plug.responses.system.getSysinfo.GetSysinfoResponse;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Client for communication with the plug.
 *
 * @author Jan Rada - jrada.cz@gmail.com
 */
public class Main {

    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    // TODO change according to the IP address of your plug
    private static final String PLUG_IP = "192.168.1.11";

    public static void main(String[] args) {
        try {

            PlugClient plugClient = new PlugClient(PLUG_IP);
            HS110Plug plug = new HS110Plug(plugClient);

            GetSysinfoResponse plugInfo = plug.getInfo();
            System.out.println(plugInfo.getJsonString());

        } catch (IOException | PlugException ex) {
            LOG.error("An error occurred", ex);
        }
    }

}
