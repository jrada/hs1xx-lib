package cz.jrada.hs1xx.plug;

import cz.jrada.hs1xx.plug.exceptions.PlugIsNotOnlineException;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Client for communication with the plug.
 *
 * @author Jan Rada - jrada.cz@gmail.com
 */
public class PlugClient {

    private static final Logger LOG = LoggerFactory.getLogger(PlugClient.class);

    private static final int DEFAULT_TCP_PORT = 9999;
//    private static final int DEFAULT_UDP_PORT = 1040; //TDDP
    protected static final int DEFAULT_TIEMOUT = 1500; // ms
    private static final byte DEFAULT_KEY = (byte) 0xAB;
    protected static final int HEADER_LENGTH = 4;

    private String ip;
    private int port;

    public PlugClient(String ip) {
        this(ip, DEFAULT_TCP_PORT);
    }

    public PlugClient(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    /**
     * Encrypts and sends a message with JSON command to the plug.
     * 
     * @param jsonCommand json string command
     * @return json string response
     * @throws IOException if the communication with the plug is not successful
     * @throws PlugIsNotOnlineException if the plug is not online
     */
    public String sendCommand(String jsonCommand) throws IOException, PlugIsNotOnlineException {
        if (!isOnline(DEFAULT_TIEMOUT)) {
            LOG.error("The plug is not online: ip={}, port={}", ip, port);
            throw new PlugIsNotOnlineException("The plug is not online");
        }

        try ( Socket socket = new Socket(ip, port);  OutputStream outputStream = socket.getOutputStream();  InputStream inputStream = socket.getInputStream()) {
            socket.setSoTimeout(DEFAULT_TIEMOUT);
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);

            // send encrypted message with json command
            bufferedOutputStream.write(getEncryptedMessageWithHeader(jsonCommand));
            bufferedOutputStream.flush();

            // read response message header
            byte[] messageHeader = bufferedInputStream.readNBytes(HEADER_LENGTH);
            int messageSize = ByteBuffer.allocate(HEADER_LENGTH).put(messageHeader).flip().getInt();
            // read and decrypt response message
            byte[] encryptedMessage = bufferedInputStream.readNBytes(messageSize);

            return getDecryptetMessage(encryptedMessage);
        }
    }

    protected boolean isOnline(int timeoutInMs) throws IOException {
        try {
            InetAddress inetAddress = InetAddress.getByName(ip);
            return inetAddress.isReachable(timeoutInMs);
        } catch (IOException ex) {
            LOG.error("Unable to check whether the plug is online: ip={}", ip, ex);
            throw ex;
        }
    }

    protected static byte[] getEncryptedMessage(String command) {
        ByteBuffer messageData = ByteBuffer.allocate(command.length());

        byte key = DEFAULT_KEY;
        byte encryptedByte;
        for (int i = 0; i < command.length(); i++) {
            encryptedByte = (byte) (command.charAt(i) ^ key);
            messageData.put(encryptedByte);
            key = encryptedByte;
        }

        return messageData.array();
    }

    protected static byte[] getEncryptedMessageWithHeader(String command) {
        // 4 bytes long message header which contains message length (characters count)
        byte[] messageHeader = ByteBuffer.allocate(HEADER_LENGTH).putInt(command.length()).array();
        // encrypted message data
        byte[] encryptedMessage = getEncryptedMessage(command);

        // concat header with message
        byte[] messageToSend = new byte[messageHeader.length + encryptedMessage.length];
        System.arraycopy(messageHeader, 0, messageToSend, 0, messageHeader.length);
        System.arraycopy(encryptedMessage, 0, messageToSend, messageHeader.length, encryptedMessage.length);

        return messageToSend;
    }

    protected static String getDecryptetMessage(byte[] bytes) throws IOException {
        StringBuilder message = new StringBuilder();

        byte key = DEFAULT_KEY;
        byte nextKey;
        for (int i = 0; i < bytes.length; i++) {
            nextKey = bytes[i];
            message.append((char) (bytes[i] ^ key));
            key = nextKey;
        }

        return message.toString();
    }

}
