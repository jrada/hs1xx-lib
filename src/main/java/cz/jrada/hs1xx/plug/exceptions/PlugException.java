package cz.jrada.hs1xx.plug.exceptions;

/**
 * @author Jan Rada - jrada.cz@gmail.com
 */
public class PlugException extends Exception {

    public PlugException (String message){
        super(message);
    }

    public PlugException (Exception ex){
        super(ex);
    }

    public PlugException (String message, Exception ex){
        super(message, ex);
    }

}
