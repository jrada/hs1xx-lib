package cz.jrada.hs1xx.plug.exceptions;

/**
 * @author Jan Rada - jrada.cz@gmail.com
 */
public class PlugIsNotOnlineException extends PlugException{

    public PlugIsNotOnlineException (String message){
        super(message);
    }

}
