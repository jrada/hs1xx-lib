package cz.jrada.hs1xx.plug.exceptions;

/**
 * @author Jan Rada - jrada.cz@gmail.com
 */
public class UnexpectedPlugResponseException extends PlugException{

    public UnexpectedPlugResponseException (Exception ex){
        super(ex);
    }

}
