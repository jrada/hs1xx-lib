package cz.jrada.hs1xx.plug.responses;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Jan Rada - jrada.cz@gmail.com
 */
public class PlugResponse {

    @JsonIgnore
    private String jsonString;

    public String getJsonString() {
        return jsonString;
    }

    public void setStringValue(String jsonString) {
        this.jsonString = jsonString;
    }

}
