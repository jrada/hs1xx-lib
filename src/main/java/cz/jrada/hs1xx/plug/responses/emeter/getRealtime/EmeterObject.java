package cz.jrada.hs1xx.plug.responses.emeter.getRealtime;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Jan Rada - jrada.cz@gmail.com
 */
public class EmeterObject {

    @JsonProperty("get_realtime")
    private GetRealtimeObject getRealtime;

    public GetRealtimeObject getGetRealtime() {
        return getRealtime;
    }

    @Override
    public String toString() {
        return "EmeterObject{" + "getRealtime=" + getRealtime + '}';
    }

}
