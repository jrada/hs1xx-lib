package cz.jrada.hs1xx.plug.responses.emeter.getRealtime;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Jan Rada - jrada.cz@gmail.com
 */
public class GetRealtimeObject {

    @JsonProperty("voltage_mv")
    private int voltage;
    @JsonProperty("current_ma")
    private int current;
    @JsonProperty("power_mw")
    private int power;
    @JsonProperty("total_wh")
    private int total;
    @JsonProperty("err_code")
    private int errCode;

    public int getVoltage() {
        return voltage;
    }

    public int getCurrent() {
        return current;
    }

    public int getPower() {
        return power;
    }

    public int getTotal() {
        return total;
    }

    public int getErrCode() {
        return errCode;
    }

    @Override
    public String toString() {
        return "GetRealtimeObject{" + "voltage=" + voltage + ", current=" + current + ", power=" + power + ", total=" + total + ", errCode=" + errCode + '}';
    }

}
