package cz.jrada.hs1xx.plug.responses.emeter.getRealtime;

import com.fasterxml.jackson.annotation.JsonProperty;
import cz.jrada.hs1xx.plug.responses.PlugResponse;

/**
 * @author Jan Rada - jrada.cz@gmail.com
 */
public class GetRealtimeResponse extends PlugResponse {

    @JsonProperty("emeter")
    private EmeterObject emeter;

    public EmeterObject getEmeter() {
        return emeter;
    }

    @Override
    public String toString() {
        return "EmeterGetRealtimeResponse{" + "emeter=" + emeter + '}';
    }

}
