package cz.jrada.hs1xx.plug.responses.system.getSysinfo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Jan Rada - jrada.cz@gmail.com
 */
public class GetSysinfoObject {

    @JsonProperty("err_code")
    private int errCode;
    @JsonProperty("sw_ver")
    private String swVer;
    @JsonProperty("hw_ver")
    private String hwVer;
    @JsonProperty("type")
    private String type;
    @JsonProperty("model")
    private String model;
    @JsonProperty("mac")
    private String mac;
    @JsonProperty("deviceId")
    private String deviceId;
    @JsonProperty("hwId")
    private String hwId;
    @JsonProperty("fwId")
    private String fwId;
    @JsonProperty("oemId")
    private String oemId;
    @JsonProperty("alias")
    private String alias;
    @JsonProperty("dev_name")
    private String devName;
    @JsonProperty("icon_hash")
    private String iconHash;
    @JsonProperty("relay_state")
    private int relayState;
    @JsonProperty("on_time")
    private long on_time;
    @JsonProperty("active_mode")
    private String active_mode;
    @JsonProperty("feature")
    private String feature;
    @JsonProperty("updating")
    private int updating;
    @JsonProperty("rssi")
    private int rssi;
    @JsonProperty("led_off")
    private int ledOff;
    @JsonProperty("latitude_i")
    private long latitude;
    @JsonProperty("longitude_i")
    private long logitude;
    @JsonProperty("next_action")
    private NextActionObject nextAction;

    public int getErrCode() {
        return errCode;
    }

    public String getSwVer() {
        return swVer;
    }

    public String getHwVer() {
        return hwVer;
    }

    public String getType() {
        return type;
    }

    public String getModel() {
        return model;
    }

    public String getMac() {
        return mac;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getHwId() {
        return hwId;
    }

    public String getFwId() {
        return fwId;
    }

    public String getOemId() {
        return oemId;
    }

    public String getAlias() {
        return alias;
    }

    public String getDevName() {
        return devName;
    }

    public String getIconHash() {
        return iconHash;
    }

    public int getRelayState() {
        return relayState;
    }

    public long getOn_time() {
        return on_time;
    }

    public String getActive_mode() {
        return active_mode;
    }

    public String getFeature() {
        return feature;
    }

    public int getUpdating() {
        return updating;
    }

    public int getRssi() {
        return rssi;
    }

    public int getLedOff() {
        return ledOff;
    }

    public long getLatitude() {
        return latitude;
    }

    public long getLogitude() {
        return logitude;
    }

    public NextActionObject getNextAction() {
        return nextAction;
    }

    @Override
    public String toString() {
        return "GetSysinfoObject{" + "errCode=" + errCode + ", swVer=" + swVer + ", hwVer=" + hwVer + ", type=" + type + ", model=" + model + ", mac=" + mac + ", deviceId=" + deviceId + ", hwId=" + hwId + ", fwId=" + fwId + ", oemId=" + oemId + ", alias=" + alias + ", devName=" + devName + ", iconHash=" + iconHash + ", relayState=" + relayState + ", on_time=" + on_time + ", active_mode=" + active_mode + ", feature=" + feature + ", updating=" + updating + ", rssi=" + rssi + ", ledOff=" + ledOff + ", latitude=" + latitude + ", logitude=" + logitude + ", nextAction=" + nextAction + '}';
    }

}
