package cz.jrada.hs1xx.plug.responses.system.getSysinfo;

import com.fasterxml.jackson.annotation.JsonProperty;
import cz.jrada.hs1xx.plug.responses.PlugResponse;

/**
 * @author Jan Rada - jrada.cz@gmail.com
 */
public class GetSysinfoResponse extends PlugResponse {

    @JsonProperty("system")
    private SystemObject system;

    public SystemObject getSystem() {
        return system;
    }

    @Override
    public String toString() {
        return "SystemGetSysinfoResponse{ system=" + system + '}';
    }

}
