package cz.jrada.hs1xx.plug.responses.system.getSysinfo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Jan Rada - jrada.cz@gmail.com
 */
public class NextActionObject {

    @JsonProperty("type")
    private int type;

    public int getType() {
        return type;
    }

    @Override
    public String toString() {
        return "NextActionObject{" + "type=" + type + '}';
    }

}
