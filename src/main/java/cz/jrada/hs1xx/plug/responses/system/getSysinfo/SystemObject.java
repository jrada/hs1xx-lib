package cz.jrada.hs1xx.plug.responses.system.getSysinfo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Jan Rada - jrada.cz@gmail.com
 */
public class SystemObject {

    @JsonProperty("get_sysinfo")
    private GetSysinfoObject getSysinfo;

    public GetSysinfoObject getGetSysinfo() {
        return getSysinfo;
    }

    @Override
    public String toString() {
        return "SystemObject{" + "getSysinfo=" + getSysinfo + '}';
    }

}
