package cz.jrada.hs1xx.plug.responses.system.setRelayState;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Jan Rada - jrada.cz@gmail.com
 */
public class SetRelayStateObject {

    @JsonProperty("err_code")
    private int errCode;

    public int getErrCode() {
        return errCode;
    }

    @Override
    public String toString() {
        return "SetRelayStateObject{" + "errCode=" + errCode + '}';
    }

}
