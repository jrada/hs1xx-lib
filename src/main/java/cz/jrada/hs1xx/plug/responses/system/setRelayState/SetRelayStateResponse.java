package cz.jrada.hs1xx.plug.responses.system.setRelayState;

import com.fasterxml.jackson.annotation.JsonProperty;
import cz.jrada.hs1xx.plug.responses.PlugResponse;

/**
 * @author Jan Rada - jrada.cz@gmail.com
 */
public class SetRelayStateResponse extends PlugResponse {

    @JsonProperty("system")
    private SystemObject system;

    public SystemObject getSystem() {
        return system;
    }

    @Override
    public String toString() {
        return "SetRelayStateResponse{" + "system=" + system + '}';
    }

}
