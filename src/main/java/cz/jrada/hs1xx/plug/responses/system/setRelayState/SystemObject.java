package cz.jrada.hs1xx.plug.responses.system.setRelayState;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Jan Rada - jrada.cz@gmail.com
 */
public class SystemObject {

    @JsonProperty("set_relay_state")
    private SetRelayStateObject setRelayState;

    public SetRelayStateObject getSetRelayState() {
        return setRelayState;
    }

    @Override
    public String toString() {
        return "SystemObject{" + "setRelayState=" + setRelayState + '}';
    }

}
