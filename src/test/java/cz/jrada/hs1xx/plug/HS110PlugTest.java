package cz.jrada.hs1xx.plug;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.jrada.hs1xx.plug.exceptions.PlugException;
import cz.jrada.hs1xx.plug.exceptions.PlugIsNotOnlineException;
import cz.jrada.hs1xx.plug.responses.emeter.getRealtime.GetRealtimeResponse;
import cz.jrada.hs1xx.plug.responses.system.getSysinfo.GetSysinfoResponse;
import cz.jrada.hs1xx.plug.responses.system.setRelayState.SetRelayStateResponse;
import java.io.IOException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Jan Rada - jrada.cz@gmail.com
 */
public class HS110PlugTest {

    private PlugClient plugClient;
    private HS110Plug plug;

    @Before
    public void setUp() throws IOException {
        plugClient = mock(PlugClient.class);
        when(plugClient.isOnline(PlugClient.DEFAULT_TIEMOUT)).thenReturn(true);
        plug = new HS110Plug(plugClient);
    }

    @Test
    public void jsonStringToJsonGetSysinfoResponse() throws IOException, PlugIsNotOnlineException, PlugException {
        String infoResponse = "{\"system\":{\"get_sysinfo\":{\"err_code\":0,\"sw_ver\":\"1.5.6 Build 191125 Rel.083657\",\"hw_ver\":\"2.0\",\"type\":\"IOT.SMARTPLUGSWITCH\",\"model\":\"HS110(EU)\",\"mac\":\"AA:AA:AA:AA:AA:AA\",\"deviceId\":\"00000000000000000000000000000003\",\"hwId\":\"00000000000000000000000000000001\",\"fwId\":\"00000000000000000000000000000002\",\"oemId\":\"00000000000000000000000000000004\",\"alias\":\"My Smart Plug\",\"dev_name\":\"Smart Wi-Fi Plug With Energy Monitoring\",\"icon_hash\":\"\",\"relay_state\":1,\"on_time\":60,\"active_mode\":\"none\",\"feature\":\"TIM:ENE\",\"updating\":0,\"rssi\":-70,\"led_off\":0,\"latitude_i\":999,\"longitude_i\":999,\"next_action\":{\"type\":-1}}}}";
        when(plugClient.sendCommand(HS110Plug.CMD_SYSTEM_GET_INFO)).thenReturn(infoResponse);

        GetSysinfoResponse response = plug.getInfo();

        ObjectMapper objectMapper = new ObjectMapper();
        String jsonString = objectMapper.writeValueAsString(response);

        Assert.assertEquals(infoResponse, jsonString);
    }

    @Test
    public void jsonStringToJsonSetRelayStateResponse() throws IOException, PlugIsNotOnlineException, PlugException {
        String turnOnResponse = "{\"system\":{\"set_relay_state\":{\"err_code\":0}}}";
        when(plugClient.sendCommand(HS110Plug.CMD_SYSTEM_TURN_ON)).thenReturn(turnOnResponse);

        SetRelayStateResponse response = plug.turnOn();

        ObjectMapper objectMapper = new ObjectMapper();
        String jsonString = objectMapper.writeValueAsString(response);

        Assert.assertEquals(turnOnResponse, jsonString);
    }

    @Test
    public void jsonStringToJsonGetRealtimeResponse() throws IOException, PlugIsNotOnlineException, PlugException {
        String getRealtimeResponse = "{\"emeter\":{\"get_realtime\":{\"voltage_mv\":237306,\"current_ma\":14,\"power_mw\":0,\"total_wh\":1,\"err_code\":0}}}";
        when(plugClient.sendCommand(HS110Plug.CMD_EMETER_REALTIME)).thenReturn(getRealtimeResponse);

        GetRealtimeResponse response = plug.getEmeterRealtime();

        ObjectMapper objectMapper = new ObjectMapper();
        String jsonString = objectMapper.writeValueAsString(response);

        Assert.assertEquals(getRealtimeResponse, jsonString);
    }

}
