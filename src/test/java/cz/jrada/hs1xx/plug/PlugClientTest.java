package cz.jrada.hs1xx.plug;

import java.io.IOException;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Jan Rada - jrada.cz@gmail.com
 */
public class PlugClientTest {

    @Test
    public void encryptDecryptTest() throws IOException {
        String originalMessage = "{\"system\":{\"get_sysinfo\":{\"sw_ver\":\"1.5.6 Build 191125 Rel.083657\",\"hw_ver\":\"2.0\",\"type\":\"IOT.SMARTPLUGSWITCH\",\"model\":\"HS110(EU)\",\"mac\":\"AA:AA:AA:AA:AA:AA\",\"dev_name\":\"Smart Wi-Fi Plug With Energy Monitoring\",\"alias\":\"My Smart Plug\",\"relay_state\":1,\"on_time\":60,\"active_mode\":\"none\",\"feature\":\"TIM:ENE\",\"updating\":0,\"icon_hash\":\"\",\"rssi\":-70,\"led_off\":0,\"longitude_i\":999,\"latitude_i\":999,\"hwId\":\"00000000000000000000000000000001\",\"fwId\":\"00000000000000000000000000000002\",\"deviceId\":\"00000000000000000000000000000003\",\"oemId\":\"00000000000000000000000000000004\",\"next_action\":{\"type\":-1},\"err_code\":0}}}";

        byte[] encryptedMessageWithoutHeader = PlugClient.getEncryptedMessage(originalMessage);
        String decryptedMessageWithoutHeader = PlugClient.getDecryptetMessage(encryptedMessageWithoutHeader);
        Assert.assertEquals(originalMessage, decryptedMessageWithoutHeader);

        byte[] encryptedMessageWithHeader = PlugClient.getEncryptedMessageWithHeader(originalMessage);

        byte[] header = new byte[PlugClient.HEADER_LENGTH];
        System.arraycopy(encryptedMessageWithHeader, 0, header, 0, PlugClient.HEADER_LENGTH);
        Assert.assertArrayEquals(new byte[] { (byte)0x00, (byte)0x00, (byte)0x02, (byte)0x5C}, header);

        int lengthWithoutHeader = encryptedMessageWithHeader.length - PlugClient.HEADER_LENGTH;
        byte[] encryptedMessageWithTrimmedHeader = new byte[lengthWithoutHeader];
        System.arraycopy(encryptedMessageWithHeader, PlugClient.HEADER_LENGTH, encryptedMessageWithTrimmedHeader, 0, lengthWithoutHeader);
        String decryptedMessageWithHeader = PlugClient.getDecryptetMessage(encryptedMessageWithTrimmedHeader);
        Assert.assertEquals(originalMessage, decryptedMessageWithHeader);
    }

}
